<?php
// Cache for Redis

class RCache extends Redis {//extends Redis

  private $debug,$is_showerror;
  public $config=array(
    'server'=>'redis',
    'debug'=>false,
    'timeout'=>2,
    'store'=>'./',
    'sync'=>array(),
  );

  function __construct($id=''){

    $c=$this->getConfig($id);
    $this->config=array_merge($this->config,$c);

    $this->debug=$this->config['debug'];
    $this->is_showerror=true;


    $server=isset($this->config['server'])?$this->config['server']:"";
    $port=empty($this->config['port'])?6379:$this->config['port'];
    $passwd=isset($this->config['passwd'])?$this->config['passwd']:"";
    $timeout=empty($this->config['timeout'])?2:$this->config['timeout'];

    parent::__construct();

    $this->pconnect($server, $port,$timeout);
    if (!empty($passwd)) $this->auth($passwd);
  }
  function getConfig($id=''){
    if (is_array($id)) return $id;
    global $Cache_Redis;

    $configarr=$Cache_Redis;
    is_array($configarr) or $this->error('config error');
    $c=array();
    if (empty($id)){
     $c=array_shift($configarr);
    }else{
     foreach ($configarr as $key => $value) {
       if ($value['id']==$id){
         $c=$value;
         break;
       }
     }
    }
    return $c;
  }
  function error($msg){
    if ($this->is_showerror){
      echo 'Cache Error : '.$msg;
      die();
    }
  }
  public function showerror($dbg=true){
    if ($dbg){
      $this->is_showerror=true;
    }else{
      $this->is_showerror=false;
    }
  }
  public function debug($dbg=true){
    if ($dbg){
      $this->debug=true;
    }else{
      $this->debug=false;
    }
    $this->showerror($dbg);
  }
  function debug_show($msg){
    if ($this->debug){
      echo "\n\n --- RCache Debug : \n";
      print_r($msg);
      echo "\n ---\n\n";
    }
  }
  function unhash($key){
    return str_replace(".","/",$key);
  }
  function sync($key,$func=false){
    $res=false;
    $valarr=false;
    if (!empty($func)){
      $valarr=$func;
    }else if (!empty($this->config['sync'][$key])){
      $valarr=$this->config['sync'][$key];
    }else{
      if (empty($this->config['store'])){
        $file='./'.$this->unhash($key).'.php';
      }else{
        $file=rtrim($this->config['store'],'/').'/'.$this->unhash($key).'.php';
      }
      if (file_exists($file)) $valarr=require($file);
    }
    $ex=false;
    if (!empty($valarr)){

      if (is_object($valarr)){
        $value=$valarr;
      }else{
        if (isset($valarr['ex'])) $ex=$valarr['ex'];
        $value=(is_array($valarr)&&isset($valarr['value']))?$valarr['value']:$valarr;
      }

      if (is_object($value)){
        $res=$value($key);
      }else{
        $res=$value;
      }
    }
    if ($res){
      if ($ex){
        $this->setEx($key,$ex,$res);
      }else{
        $this->set($key,$res);
      }
    }

    return $res;
  }
  function get($key,$default=false){
    $val=parent::get($key);

    if ($val===false){
      $val=$this->sync($key,$default);
    }
    return $val;

  }
  function setSync($conf){
    $this->config['sync']=array_merge;
  }
  function setStore($store){
    $this->config['store']=$store;
  }




}


?>
