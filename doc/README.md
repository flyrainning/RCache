# RCache

RCache提供了对Redis的扩展功能，包括配置和统一的数据更新功能

## Redis服务器配置

Redis服务器可以使用全局配置，或直接使用配置数组实例化对象

RCache继承自PHP的Redis对象

### 全局配置

设定全局变量`$Cache_Redis`，支持多服务器，$Cache_Redis是一个二维数组，每一个元素包含一个服务器的配置信息，通过`id`对服务器进行区分，创建对象时通过id指定要使用的服务器

```
$Cache_Redis=array(
  array(
    'id'=>'1',
    'server' => 'redis1',
    'passwd' => '',
  ),
  array(
    'id'=>'2',
    'server' => 'localhost',
    'port'=> 123,
    'passwd' => '123',
  ),
);

//连接redis1
$cache=new RCache("1");

//连接配置2，服务器localhost
$cache2=new RCache("2");

```


## 数据更新

RCache最大的特点是可以通过集中配置，实现缓存的自动更新，这样，在项目中可以无需处理缓存未命中的逻辑

自动更新可以通过`同步配置`、`同步文件`和`默认值`三种方式实现



### 同步配置

通过加载同步配置数组
