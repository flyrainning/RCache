<?php

$cache=new RCache(array(
  'server' => 'redis',
));


$cache->delete("key1");
$cache->delete("key2");
$cache->delete("key3");
$cache->delete("group1.key4");
$cache->delete("group1/key4");

$sync_dir=__DIR__.'/store';

$cache->setStore($sync_dir);


$res=$cache->get("key1");
print_r($res);



$res=$cache->get("key2");
print_r($res);



$res=$cache->get("key3");
print_r($res);


$res=$cache->get("group1.key4");
print_r($res);


$res=$cache->get("group1/key4");
print_r($res);
 ?>
