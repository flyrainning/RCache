<?php

$cache=new RCache(array(
  'server' => 'redis',
));


$cache->delete("key1");
$cache->delete("key2");
$cache->delete("key3");


$sync_config=array(
  'key1'=>'value1',
  'key2'=>function(){
    return 1234;
  },
  'key3'=>array(
    'ex'=>10,
    'value'=>function(){
      return "value3";
    }
  ),
);

$cache->setSync($sync_config);


$res=$cache->get("key1");
print_r($res);



$res=$cache->get("key2");
print_r($res);



$res=$cache->get("key3");
print_r($res);

 ?>
